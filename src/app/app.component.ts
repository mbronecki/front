import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  data = JSON.parse(`[
    {
        "id": 1,
        "name": "A",
        "level": 1,
        "nodes": [
            {
                "id": 2,
                "name": "AA",
                "level": 2,
                "nodes": [
                    {
                        "id": 3,
                        "name": "AA1",
                        "level": 3,
                        "nodes": []
                    },
                    {
                        "id": 4,
                        "name": "AA2",
                        "level": 3,
                        "nodes": []
                    }
                ]
            },
            {
                "id": 5,
                "name": "AB",
                "level": 2,
                "nodes": []
            }
        ]
    },
    {
        "id": 6,
        "name": "B",
        "level": 1,
        "nodes": []
    },
    {
        "id": 7,
        "name": "C",
        "level": 1,
        "nodes": [
            {
                "id": 8,
                "name": "CA",
                "level": 2,
                "nodes": [
                    {
                        "id": 9,
                        "name": "CA1",
                        "level": 3,
                        "nodes": []
                    },
                    {
                        "id": 10,
                        "name": "CA2",
                        "level": 3,
                        "nodes": []
                    }
                ]
            }
        ]
    },
    {
        "id": 11,
        "name": "D",
        "level": 1,
        "nodes": [
            {
                "id": 12,
                "name": "DA",
                "level": 2,
                "nodes": []
            }
        ]
    }
]`)
}
